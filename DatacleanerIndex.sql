#################################################
# DATA CLEANER PROJECT: INDEX MAKER
# This code creates an index for commodity trades from Comtrade, Fishstat(FAO), and Tradestat(FAO)
#	This must be run before running datacleaner.region
# comtrade_17_v1_sql <- use the sql schema
# tables needed 
#	`country_data`.`country_matrix_2018`
#	`faostat_18_v2_sql`.`tradestat` <- update
#
# tables created:
#
#	`faostat_18_v2_sql`.`index_maker_world`
#	`faostat_18_v2_sql`.`index_maker_region`
#
# Update: the latest year should be updated in where clause.
#
#################################################


#############
# FAOSTAT
#############

USE `faostat_18_v2_sql`\p;

DROP TABLE IF EXISTS index_maker_world\p;
CREATE TABLE index_maker_world
( year INT,
  element_ID int NOT NULL,
  UN_region varchar(100) NOT NULL,
  FAO_item_code int NOT NULL,
  next_value numeric(20,2),
  cur_value numeric(20,2),
  growth_index double,
  FFIL_index double,
  CONSTRAINT PK_pr PRIMARY KEY (year,element_ID,fao_item_code,UN_region)
)\p;


# index_maker_region (with all element_ID being grouped)
DROP TABLE IF EXISTS index_maker_region\p;
CREATE TABLE index_maker_region 
( year INT,
  element_ID int NOT NULL,
  UN_region varchar(100) NOT NULL,
  FAO_item_code int NOT NULL,
  next_value numeric(20,2),
  cur_value numeric(20,2),
  growth_index double,
  FFIL_index double,
  CONSTRAINT PK_pr PRIMARY KEY (year,element_ID,fao_item_code,UN_region)
)\p;

# Regional Trade
INSERT INTO `faostat_18_v2_sql`.index_maker_region
SELECT ft.*,ft.cur_value/ft.next_value AS growth_index, ft.next_value/ft.cur_value AS FFIL_index
FROM 
	(SELECT wow.year, wow.element_ID,wow.UN_region,wow.FAO_item_code, @r AS `next_value`, (@r := value) AS `cur_value`
	FROM (
		SELECT c.*
		FROM 
		(SELECT @code = NULL,
			@region = NULL,
            @element = NULL
			) vars,
		(SELECT a.year,a.element_ID,c.UN_region,a.FAO_item_code, SUM(a.value) AS value
		FROM `faostat_18_v2_sql`.tradestat a 
		LEFT JOIN country_data.country_matrix_2018 c
		ON a.country_code = c.country_code
		GROUP BY UN_region,year,FAO_item_code,element_ID
		ORDER BY UN_region,FAO_item_code,element_ID,year DESC) c) wow
	WHERE (CASE WHEN (@code <> FAO_item_code AND @region <> UN_region AND @element <> element_ID) THEN 
			(@r := NULL) ELSE NULL END IS NULL) AND
			(@code := FAO_item_code AND @region := UN_region AND @element := element_ID) IS NOT NULL) ft
WHERE year < 2014
ORDER BY UN_region,FAO_item_code,element_ID, year DESC\p;

