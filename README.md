# README #


### What is this repository for? ###

* This repository holds all of the scripts one needs to finish the data processing for the faostat schemas once the upload script has been successfully run.
* Specifically, the tradestat table has to go through further processing before reaching it's final version in the "ult" schema.
* new\_tradestat\_dc.R is the only script that's needed, others are here for legacy documentation
* As of implementation for NFA 2018, the script includes a hardcoded naive front-fill from data year 2013 to 2014. To be removed or edited as needed for later editions.

### Order and execution of scripts: ###

- new\_tradestat\_dc.R
	- This script includes the functions and methods of the other scripts. 
	- replace the relevant schema name. If there is no matlab datacleaned table with which to ensemble, then comment out the matlab datacleaning part.
	
To maintain clarity, these are the steps that include multiple scripts pre-consolidation into new\_tradestat\_dc.

1. DatacleanerIndex.sql
	- This script sets up a table of region-commodity-year indices for back- and front-filling. The index\_maker\_world table is filled by the next script.
	- Find and replace table names, including country\_matrix\__year_. In the insert statement for index\_maker\_region, change the year to that after the most recent data year in your tradestat table.
1. tradestat\_dc.R
	- This script does linear interpolation between real observations, and applies back- and front-filling indices starting from the closest real observation. Also adds dcflag column.
	- Find and replace relevant table names, including country\_matrix\__year_
	- In the insert statement for index\_maker\_world, change the year to that after the latest data year.
1. Post\_dataclean\_ensemble\_FFIL\_aggsplit.sql
	- This script is here to: 
		- Average the tradestat\_dc.R output with that of a different datacleaner that may not be available.
		- Naively front fill data to latest NFA data year (if needed).
		- Aggregate relevant countries into 'China' and put them into the 'hk\_etc' schema.
	- Find and replace the names of relevant tables. 
	
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)