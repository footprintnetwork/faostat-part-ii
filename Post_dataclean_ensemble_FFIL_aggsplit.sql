################
#	This script (a) ensembles commodity-level timelines for a few countries with matlab-datacleaned version (may not be relevant in future NFAs) 
#				(b) Frontfills to 2014 if necessary (relevant for NFA 2018, perhaps not in the future)
#				(c) Creates china aggsplit schema & table*  
#	
#	Find and replace table names (where relevant)
#	Find and replace: (with filepath to your .out file)
#		C:\Users\Evan Neill.ECOFOOT\Dropbox (Footprint Network)\GFN NFA\NFA 2018\2. Datasets\Other\Upload Data\dot out log files\HK_split_dataclean.out
#
#	INPUT TABLES  
#		faostat_18_v2_ult.tradestat_r_dt_ult_test 		# Output of R datacleaner
#		faostat_17_v1_ult.tradestat_ult 				# This is a matlab-datacleaned version.
#
#		We used the '17 matlab datacleaned version for the ensemble because it shared the same end year 
#	`	with the available download of tradestat, and we didn't have matlab available for datacleaning purposes.
#		The ensemble part may be impossible in future years - would have to either ameliorate country changes or alter datacleaner
#		to fix it. 
#
#	OUTPUT TABLES  
#		faostat_18_v2_ult.tradestat_ensemble
#		faostat_18_v2_ult_hketc.tradestat_ensemble
#
#	Data Flags - 
#		CnAg - Aggregated data for china
#		NAIV - Naively frontfilled
#
#	* This year, FAO tradestat reports all of china as well as Taiwan, mainland china, hong kong, and macao.
#	As such, I've commented out the 'insert into' part, since we only need the transfer to the ult_hketc schema part.
#	 -EN 10/4/17
#################

#TEE C:\Users\Evan Neill.ECOFOOT\Dropbox (Footprint Network)\GFN NFA\NFA 2018\2. Datasets\Other\Upload Data\dot out log files\HK_split_dataclean.out
TEE C:\Users\Evan Neill.ECOFOOT\Dropbox (Footprint Network)\GFN Docs\Staff Folders\Evan Neill\FAO_upload_2018\HK_split_dataclean.out
WARNINGS\p;

SET @total_time=UNIX_TIMESTAMP();
SELECT NOW() as 'Data Load Started';

###########################################################################################
# Ensemble with matlab datacleaned set if applicable
###########################################################################################

USE `faostat_18_v2_ult`;
DROP TABLE IF EXISTS `tradestat_ensemble`;
Create table `tradestat_ensemble` like `tradestat_r_ult`\p;
insert into `tradestat_ensemble`
(SELECT 
    a.country_code,
    a.country,
    a.FAO_item_code,
    a.item,
    a.element_ID,
    a.subject,
    a.year,
    a.unit,
    IF((country_code in (26, 48, 179, 225) AND a.GFN_flag !='R'), (ifnull(a.value,0) + ifnull(b.value,0))/2, ifnull(a.value,0)),
    a.flag,
    a.GFN_flag
FROM
    faostat_18_v2_ult.tradestat_r_ult a
    left join `faostat_17_v1_ult`.`tradestat_ult` b
    using (country_code, fao_item_code, element_id, year));

###########################################################################################
# Frontfill naively to 2014 if necessary
###########################################################################################

use faostat_18_v2_ult;

Drop table if exists tradestat_ensemble_naive2014;
create table tradestat_ensemble_naive2014 like faostat_18_v2_ult.tradestat_ensemble;
Insert into tradestat_ensemble_naive2014 select *  from faostat_18_v2_ult.tradestat_ensemble;

insert into tradestat_ensemble_naive2014 select country_code, country, fao_item_code, item, element_id, subject, '2014', unit, value, flag, 'NAIV'  
from tradestat_ensemble where year =2013 and country_code!=206; # Except for Sudan (former), which for this year already went to 2014

###########################################################################################
# AGGREGATE CHINA IN DATACLEANED SCHEMAS
###########################################################################################


/*insert into faostat_18_v2_ult.tradestat_ensemble
select 351,'China',FAO_item_code,item,element_ID,subject,year,unit,sum(value),flag,'CnAG'
from faostat_18_v2_ult.tradestat_ensemble
WHERE country_code IN (41,96,214,128)
GROUP BY year,element_ID,FAO_item_code ;*/

###########################################################################################
# SPLIT HK_ETC FROM DATACLEANED SCHEMAS
###########################################################################################
USE faostat_18_v2_ult_hketc \p;

DROP TABLE IF EXISTS tradestat_ensemble_naive2014\p;
CREATE TABLE tradestat_ensemble_naive2014 LIKE faostat_18_v2_ult.tradestat_ensemble_naive2014\p;
INSERT INTO tradestat_ensemble_naive2014 SELECT * FROM faostat_18_v2_ult.tradestat_ensemble_naive2014
WHERE country_code in (41, 96, 128, 214)\p;

DELETE FROM faostat_18_v2_ult.tradestat_ensemble_naive2014
WHERE country_code in (41, 96, 128, 214)\p;


NOTEE